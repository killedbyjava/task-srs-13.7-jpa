package com.kj.jpa.demo;

import com.kj.jpa.demo.model.Catalog;
import com.kj.jpa.demo.repository.CatalogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpaDemoApplication implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(JpaDemoApplication.class);


	//Инжектирование маппера:
	@Autowired
	private CatalogRepository catalogRepository;

	public static void main(String[] args) {
		SpringApplication.run(JpaDemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		log.info("StartApplication...");

		//Здесь использованы методы из CrudRepository, нам их уже не надо реализовывать для последующего применения:
		//Сохранение в таблицу
		//catalogRepository.save(new Catalog("Cars"));
		//catalogRepository.save(new Catalog("Flyes"));
		//catalogRepository.save(new Catalog("Trains"));

		System.out.println("\nfindAll()");
		//Вывод данных таблиц в цикле:
		catalogRepository.findAll().forEach(x -> System.out.println(x));

		System.out.println("\nfindById(1L)");
		//Поиск по ID:
		catalogRepository.findById(1l).ifPresent(x -> System.out.println(x));

		/*System.out.println("\nfindByName('Cars')");
		//Поиск по имени:
		catalogRepository.findByName("Cars").forEach(x -> System.out.println(x));*/

		System.out.println("\nfindByLikeName('%s')");
		//Поиск по имени:
		catalogRepository.findByLikeName("%s").forEach(x -> System.out.println(x));
	}
}
