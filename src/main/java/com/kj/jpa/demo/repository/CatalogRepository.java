package com.kj.jpa.demo.repository;

import com.kj.jpa.demo.model.Catalog;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CatalogRepository extends CrudRepository<Catalog, Long> {

    List<Catalog> findByName(String name);

    //Hibernate использует собственный диалект SQL, не идентичный диалекту Postgres:
    //Используется не название таблицы, а название класса, с которым она связана. То же относится и к названию полей:
    @Query("From Catalog c WHERE c.name LIKE :name")
    List<Catalog> findByLikeName(@Param("name") String name);

}